package bean;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.PedidoDAO;
import entidade.Pedido;
import exception.ErroSistema;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@SessionScoped
public class PedidoBean {
    
    private Pedido pedido = new Pedido();
    private List<Pedido> pedidos = new ArrayList<>();
    private PedidoDAO pedidoDAO = new PedidoDAO();
    
    public void adicionar(){
        try {
            pedidoDAO.salvar(pedido);
            pedido = new Pedido();
            adicionarMensagem("Salvo!", "Pedido salva com sucesso!", FacesMessage.SEVERITY_INFO);
        } catch (ErroSistema ex) {
            adicionarMensagem(ex.getMessage(), ex.getCause().getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
    
    public void listar(){
        try {
            pedidos = pedidoDAO.buscar();
            if(pedidos == null || pedidos.size() == 0){
                adicionarMensagem("Nenhum dado encontrado!", "Sua busca não retornou nenhum pedido!", FacesMessage.SEVERITY_WARN);
            }
        } catch (ErroSistema ex) {
            adicionarMensagem(ex.getMessage(), ex.getCause().getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
  
    public void deletar(Pedido p){
        try {
            pedidoDAO.deletar(p.getId());
            adicionarMensagem("Deletado!", "Pedido deletado com sucesso!", FacesMessage.SEVERITY_INFO);
        } catch (ErroSistema ex) {
            adicionarMensagem(ex.getMessage(), ex.getCause().getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
    
    public void editar(Pedido p){
        pedido = p;
    }

    public void adicionarMensagem(String sumario, String detalhe, FacesMessage.Severity tipoErro){
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(tipoErro, sumario, detalhe);
        context.addMessage(null, message);
    }
    
    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
    public void gerarPDFfornecedor() throws ErroSistema {
        PedidoDAO ped= new PedidoDAO();
        Document doc = new Document();
        List<Pedido> listp = ped.buscar();
        String arquivoPdf = "relatorioFornecedores.pdf";

        try {
            PdfWriter.getInstance(doc, new FileOutputStream(arquivoPdf));
            doc.open();

            Paragraph p = new Paragraph("Relatório para Fornecedores");
            p.setAlignment(1);
            doc.add(p);
            p = new Paragraph("  ");
            doc.add(p);

            PdfPTable table = new PdfPTable(3);

            PdfPCell cel1 = new PdfPCell(new Paragraph("Titulo"));
            PdfPCell cel2 = new PdfPCell(new Paragraph("Quantidade"));
            PdfPCell cel3 = new PdfPCell(new Paragraph("Valor"));

            table.addCell(cel1);
            table.addCell(cel2);
            table.addCell(cel3);

            for (Pedido pedido : listp) {
                cel1 = new PdfPCell(new Paragraph(pedido.getTitulo()+""));
                cel2 = new PdfPCell(new Paragraph(pedido.getQuantidade()+""));
                cel3 = new PdfPCell(new Paragraph(pedido.getValor()+""));

                table.addCell(cel1);
                table.addCell(cel2);
                table.addCell(cel3);
            }
            
            doc.add(table);
            doc.close();
            Desktop.getDesktop().open(new File(arquivoPdf));
        } catch (Exception e) {
        }
    }
    public void gerarPDFclientes() throws ErroSistema  {
        PedidoDAO ped2= new PedidoDAO();
        Document doc = new Document();
        List<Pedido> listp = ped2.buscar();
        String arquivoPdf = "relatorioClientes.pdf";

        try {
            PdfWriter.getInstance(doc, new FileOutputStream(arquivoPdf));
            doc.open();

            Paragraph p = new Paragraph("Relatório Pedidos Clientes");
            p.setAlignment(1);
            doc.add(p);
            p = new Paragraph("  ");
            doc.add(p);

            PdfPTable table = new PdfPTable(9);

            PdfPCell cel1 = new PdfPCell(new Paragraph("ID"));
            PdfPCell cel2 = new PdfPCell(new Paragraph("Nome"));
            PdfPCell cel3 = new PdfPCell(new Paragraph("Endereço"));
            PdfPCell cel4 = new PdfPCell(new Paragraph("Telefone"));
            PdfPCell cel5 = new PdfPCell(new Paragraph("Titulo"));
            PdfPCell cel6 = new PdfPCell(new Paragraph("Descrição"));
            PdfPCell cel7 = new PdfPCell(new Paragraph("Quantidade"));
            PdfPCell cel8 = new PdfPCell(new Paragraph("Valor"));
            PdfPCell cel9 = new PdfPCell(new Paragraph("Data"));

            table.addCell(cel1);
            table.addCell(cel2);
            table.addCell(cel3);
            table.addCell(cel4);
            table.addCell(cel5);
            table.addCell(cel6);
            table.addCell(cel7);
            table.addCell(cel8);
            table.addCell(cel9);

            for (Pedido pedido : listp) {
                cel1 = new PdfPCell(new Paragraph(pedido.getId()+""));
                cel2 = new PdfPCell(new Paragraph(pedido.getNome()+""));
                cel3 = new PdfPCell(new Paragraph(pedido.getEndereco()+""));
                cel4 = new PdfPCell(new Paragraph(pedido.getTelefone()+""));
                cel5 = new PdfPCell(new Paragraph(pedido.getTitulo()+""));
                cel6 = new PdfPCell(new Paragraph(pedido.getDescricao()+""));
                cel7 = new PdfPCell(new Paragraph(pedido.getQuantidade()+""));
                cel8 = new PdfPCell(new Paragraph(pedido.getValor()+""));
                cel9 = new PdfPCell(new Paragraph(pedido.getData()+""));

                table.addCell(cel1);
                table.addCell(cel2);
                table.addCell(cel3);
                table.addCell(cel4);
                table.addCell(cel5);
                table.addCell(cel6);
                table.addCell(cel7);
                table.addCell(cel8);
                table.addCell(cel9);
            }
            
            doc.add(table);
            doc.close();
            Desktop.getDesktop().open(new File(arquivoPdf));
        } catch (Exception e) {
        }
    }

}
