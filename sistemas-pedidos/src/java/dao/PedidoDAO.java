package dao;

import bean.PedidoBean;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import entidade.Pedido;
import util.Conexao;
import exception.ErroSistema;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guigo
 */
public class PedidoDAO {

    public void salvar(Pedido pedido) throws ErroSistema {
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps;
            if (pedido.getId() == null) {
                ps = conexao.prepareStatement("INSERT INTO `pedido`( `nome`, `endereco`, `telefone`, `titulo`, `descricao`, `quantidade`, `valor`, `data`) VALUES (?,?,?,?,?,?,?,?)");
            }else{
               ps = conexao.prepareStatement("UPDATE pedido SET nome=?, endereco=?, telefone=?, titulo=?, descricao=?, quantidade=?, valor=?, data=? where id=?");
               
                ps.setInt(9, pedido.getId());
            }
               ps.setString(1, pedido.getNome());
                ps.setString(2, pedido.getEndereco());
                ps.setString(3, pedido.getTelefone());
                ps.setString(4, pedido.getTitulo());
                ps.setString(5, pedido.getDescricao());
                ps.setInt(6, pedido.getQuantidade());
                ps.setInt(7, pedido.getValor());
                ps.setDate(8, new Date(pedido.getData().getTime()));
                ps.execute();  
            
            Conexao.fecharConexao();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao tentar salvar!", ex);
        }
    }
   

    public void deletar(Integer idPedido) throws ErroSistema {
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("delete from pedido where id = ?");
            ps.setInt(1, idPedido);
            ps.execute();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar um pedido!", ex);
        }
    }

    public List<Pedido> buscar() throws ErroSistema {
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("select * from pedido");
            ResultSet resultSet = ps.executeQuery();
            List<Pedido> pedidos = new ArrayList<>();
            while (resultSet.next()) {
                Pedido pedido = new Pedido();
                pedido.setId(resultSet.getInt("id"));
                pedido.setNome(resultSet.getString("nome"));
                pedido.setEndereco(resultSet.getString("endereco"));
                pedido.setTelefone(resultSet.getString("telefone"));
                pedido.setTitulo(resultSet.getString("titulo"));
                pedido.setDescricao(resultSet.getString("descricao"));
                pedido.setQuantidade(resultSet.getInt("quantidade"));
                pedido.setValor(resultSet.getInt("valor"));
                pedido.setData(resultSet.getDate("data"));
                pedidos.add(pedido);
            }
            Conexao.fecharConexao();
            return pedidos;

        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar um pedido!", ex);
        }
    }
    
}
